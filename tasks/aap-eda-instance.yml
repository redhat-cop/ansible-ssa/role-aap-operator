---
- name: Set Automation EDA instance variables
  ansible.builtin.set_fact:
    aapeda_instance_name: "{{ aap_eda_instance_name }}"
    aapeda_instance_route: "{{ aap_eda_instance_name }}.{{ ocp_ingress_domain[0] }}"

- name: Create Automation EDA admin password secret
  block:
    - name: Set Automation EDA instance admin password
      ansible.builtin.set_fact:
        aapeda_instance_admin_password: "{{ aap_eda_admin_password }}"

    - name: Create Automation EDA admin password secret
      kubernetes.core.k8s:
        definition:
          api_version: v1
          metadata:
            name: "{{ aapeda_instance_name }}-admin-password"
            namespace: "{{ aap_operator_namespace }}"
          kind: Secret
          label_selectors:
            - "app.kubernetes.io/name={{ aapeda_instance_name }}"
          data:
            password: "{{ aapeda_instance_admin_password | b64encode }}"
        state: present
  when: aap_eda_admin_password is defined

- name: Deploy Automation EDA instance
  kubernetes.core.k8s:
    state: present
    template:
      - path: "{{ aap_eda_instance_path }}/aap-eda-instance.yml.j2"

- name: Wait till Automation EDA instance is fully deployed
  kubernetes.core.k8s_info:
    api_version: eda.ansible.com/v1alpha1
    kind: EDA
    name: "{{ aapeda_instance_name }}"
    namespace: "{{ aap_operator_namespace }}"
  register: aapeda_instance_obj
  until:
    - aapeda_instance_obj.resources[0].status is defined
    - aapeda_instance_obj.resources[0].status |
       json_query('conditions[?type==`Successful`].status') == ["True"]
    - aapeda_instance_obj.resources[0].status |
       json_query('conditions[?type==`Running`].status') == ["True"]
  retries: 85
  delay: 10

- name: Get Automation EDA admin password secret
  kubernetes.core.k8s_info:
    api_version: v1
    kind: Secret
    name: "{{ aapeda_instance_name }}-admin-password"
    namespace: "{{ aap_operator_namespace }}"
    label_selectors:
      - "app.kubernetes.io/name={{ aapeda_instance_name }}"
  register: aapeda_secret_admin_passwd

- name: Get Automation EDA admin password
  ansible.builtin.set_fact:
    aapeda_admin_passwd: "{{ aapeda_secret_admin_passwd | json_query('resources[*].data.password') | b64decode }}"

- name: Print Automation EDA url and admin password
  ansible.builtin.debug:
    msg:
    - "Automation EDA URL: https://{{ aapeda_instance_route }}"
    - "Autoamtion EDA Admin Credentials: admin / {{ aapeda_admin_passwd }}"
